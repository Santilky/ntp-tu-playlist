import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

let initialState = {
    username: null,
    token: null,
    playlist: [],
    generosMusica: [],
    valgen: [],
    valsong: [],
    register: true,
    address: null,
    id: null,
    temaactual: null,
    ultimaValoracion: 0,
    valsongActual: null
}

const Store = new Vuex.Store({
    state: initialState,
    mutations: {
        login: function (state, paquete) {
            state.username = paquete.username
            state.token = paquete.token
            state.id = paquete.id
        },
        logres: function (state) {
            state.register = !state.register
        },
        logout: function (state) {
            state.username = null
            state.token = null
            state.playlist = []
            state.generosMusica = []
            state.valgen = []
            state.valsong = []
            state.register = true
            state.address = null
            state.id = null
            state.temaactual = null
        },
        setAddress: function (state, address) {
            state.address = address
        },
        setValGen: function (state, generos) {
            state.valgen = generos
        },
        addvalgen: function (state, genero) {
            const gen = state.valgen.find(o => o.genId === genero.genId)
            if (gen !== null || gen !== undefined) {
                gen.score = genero.score
                        } else {
                state.valgen.push(gen)
            }
        },
        setValSongs: function (state, valcanciones) {
            state.valsong = valcanciones
        },
        cargarPlayList: function (state, playList) {
            for (const tema of playList) {
                state.playlist.push(tema)
            }
        },
        NextTrack: function (state) {
            let obj = null
            if (state.playlist.length > 0) {
                obj = state.playlist.shift()
                state.temaactual = obj
                state.valsongActual = obj.id
                state.address = 'http://localhost:8000/api/songs/?songid=' + obj.id + '&token=' + state.token
                if (state.playlist.length === 0) {
               //     console.log('Nos quedamos sin canciones!!!')
                    const token = state.token
                    const uri = `http://localhost:8000/api/playlist/` + token
                    fetch(uri, {
                      method: 'GET',
                      headers: {
                      'Content-Type': 'application/json'
                    } }).then(response => {
                      if (response.status === 200) {
                        return response.json()
                      }
                    })
                    .then(datos => {
                      //  console.log('conseguimos mas!')
                        for (const tema of datos) {
                            state.playlist.push(tema)
                            console.log(tema)
                        }
                    }).catch(error => {
                      //  console.log(error)
                      //  return obj
                      if (error) {
                      }
                    })
                }
            }
            state.temaactual = obj
            return obj
        },
        temaActual: function (state, tema) {
            state.tema = tema
        }
    },
    getters: {
        isLogged: function (state) {
            return state.username != null && state.username.length > 0
        },
        getGeneros: function (state) {
            return state.generosMusica
        },
        LogResPage: function (state) {
            return state.register
        },
        getPlaylist: function (state) {
            return state.playlist
        },
        getValGen: function (state) {
            return state.valgen
        },
        getValSongs: function (state) {
            return state.valsong
        },
        getMaxValGen: function (state) {
            let i = 0
            for (const genero of state.valgen) {
           //     console.log(genero)
                if (genero !== undefined || genero !== null) {
                    i += genero.score
                }
            }
            return i
        },
        getAddress: function (state) {
        //    console.log('Address:')
        //    console.log(state.address)
            return state.address
        },
        getToken: function (state) {
            return state.token
        },
        getUserName: function (state) {
            return state.username
        },
        getTemaActual: function (state) {
            return state.temaactual
        },
        getId: function (state) {
            return state.id
        }
    }
})

export default Store
